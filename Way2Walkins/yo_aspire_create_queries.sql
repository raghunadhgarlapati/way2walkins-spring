-- MySQL dump 10.13  Distrib 5.7.24, for Win64 (x86_64)
--
-- Host: localhost    Database: yo_aspire_db
-- ------------------------------------------------------
-- Server version	5.7.24-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `drive_employer_tbl`


--

DROP TABLE IF EXISTS `drive_employer_tbl`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `drive_employer_tbl` (
  `drive_id` int(11) NOT NULL,
  `employer_id` int(11) NOT NULL,
  KEY `FKbijcn4qgm1ci4wuvsv26sma36` (`drive_id`),
  KEY `FKm5u0otcyn6tm3qbyfjip70c9n` (`employer_id`),
  CONSTRAINT `FKbijcn4qgm1ci4wuvsv26sma36` FOREIGN KEY (`drive_id`) REFERENCES `drive_tbl` (`drive_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FKm5u0otcyn6tm3qbyfjip70c9n` FOREIGN KEY (`employer_id`) REFERENCES `user_tbl` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `drive_employer_tbl`
--



--
-- Table structure for table `drive_feed_back_details`
--

DROP TABLE IF EXISTS `drive_feed_back_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `drive_feed_back_details` (
  `drive_id` int(11) NOT NULL,
  `feedback_details_id` int(11) NOT NULL,
  KEY `FK58sfxcdh55compu82ly3brh0n` (`feedback_details_id`),
  KEY `FKc0vk8mkhgedum92lwg3ufhp7n` (`drive_id`),
  CONSTRAINT `FK58sfxcdh55compu82ly3brh0n` FOREIGN KEY (`feedback_details_id`) REFERENCES `feedback_details_tbl` (`feedback_details_id`) ON UPDATE CASCADE,
  CONSTRAINT `FKc0vk8mkhgedum92lwg3ufhp7n` FOREIGN KEY (`drive_id`) REFERENCES `drive_tbl` (`drive_id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `drive_feed_back_details`
--



--
-- Table structure for table `drive_interviewer_tbl`
--

DROP TABLE IF EXISTS `drive_interviewer_tbl`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `drive_interviewer_tbl` (
  `drive_id` int(11) NOT NULL,
  `interviewer_id` int(11) NOT NULL,
  KEY `FK9cgaiyecbjp7l5dith14a7yjm` (`drive_id`),
  KEY `FKlo1kkq4jt439o75ave2h5ayf2` (`interviewer_id`),
  CONSTRAINT `FK9cgaiyecbjp7l5dith14a7yjm` FOREIGN KEY (`drive_id`) REFERENCES `drive_tbl` (`drive_id`) ON UPDATE CASCADE,
  CONSTRAINT `FKlo1kkq4jt439o75ave2h5ayf2` FOREIGN KEY (`interviewer_id`) REFERENCES `user_tbl` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `drive_interviewer_tbl`
--

LOCK TABLES `drive_interviewer_tbl` WRITE;
/*!40000 ALTER TABLE `drive_interviewer_tbl` DISABLE KEYS */;
/*!40000 ALTER TABLE `drive_interviewer_tbl` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `drive_tbl`
--

DROP TABLE IF EXISTS `drive_tbl`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `drive_tbl` (
  `drive_id` int(11) NOT NULL AUTO_INCREMENT,
  `created_by` int(11) NOT NULL,
  `city` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `description` varchar(255) CHARACTER SET latin1 NOT NULL,
  `eligibility` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `exactly` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `feedback_format` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `heading` varchar(255) CHARACTER SET latin1 NOT NULL,
  `image` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `is_conducted` tinyint(4) NOT NULL DEFAULT '0',
  `is_deleted` tinyint(4) NOT NULL DEFAULT '0',
  `more_information` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `phone` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `register_before` date DEFAULT NULL,
  `sub_heading` varchar(255) CHARACTER SET latin1 NOT NULL,
  `tags` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `time_stamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `url` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `when_created` date NOT NULL,
  `where_created` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  PRIMARY KEY (`drive_id`),
  KEY `FK6t5g4swpntt1cxh9grtbgq5i6` (`created_by`),
  CONSTRAINT `FK6t5g4swpntt1cxh9grtbgq5i6` FOREIGN KEY (`created_by`) REFERENCES `user_tbl` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=euckr;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `drive_tbl`
--


--
-- Table structure for table `drive_tbl_registrations`
--

DROP TABLE IF EXISTS `drive_tbl_registrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `drive_tbl_registrations` (
  `drive_drive_id` int(11) NOT NULL,
  `registrations_reg_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `drive_tbl_registrations`
--

LOCK TABLES `drive_tbl_registrations` WRITE;
/*!40000 ALTER TABLE `drive_tbl_registrations` DISABLE KEYS */;
/*!40000 ALTER TABLE `drive_tbl_registrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `feedback_details_tbl`
--

DROP TABLE IF EXISTS `feedback_details_tbl`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `feedback_details_tbl` (
  `feedback_details_id` int(11) NOT NULL AUTO_INCREMENT,
  `comment` varchar(255) DEFAULT NULL,
  `cutoff_rating` int(11) NOT NULL,
  `max_rating` int(11) NOT NULL,
  `rating` int(11) DEFAULT NULL,
  `skill` varchar(255) NOT NULL,
  `skill_category` int(11) NOT NULL,
  `feed_back_feedback_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`feedback_details_id`),
  KEY `fdk_idx` (`feed_back_feedback_id`),
  CONSTRAINT `FKbhgkrn3i9v7inxf7a67by1a34` FOREIGN KEY (`feed_back_feedback_id`) REFERENCES `feedback_tbl` (`feedback_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `feedback_details_tbl`
--

--
-- Table structure for table `feedback_tbl`
--

DROP TABLE IF EXISTS `feedback_tbl`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `feedback_tbl` (
  `feedback_id` int(11) NOT NULL AUTO_INCREMENT,
  `comments` varchar(255) NOT NULL,
  `interview_video` varchar(255) DEFAULT NULL,
  `is_recommended` bit(1) NOT NULL,
  `interview_interview_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`feedback_id`),
  KEY `FKsl6u4orf91hijwowuyswxgv3j` (`interview_interview_id`),
  CONSTRAINT `FKsl6u4orf91hijwowuyswxgv3j` FOREIGN KEY (`interview_interview_id`) REFERENCES `interview` (`interview_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `feedback_tbl`
--



--
-- Table structure for table `interview`
--

DROP TABLE IF EXISTS `interview`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `interview` (
  `interview_id` int(11) NOT NULL AUTO_INCREMENT,
  `exactly` varchar(255) DEFAULT NULL,
  `is_conducted` tinyint(4) NOT NULL,
  `when_interviewed` date NOT NULL,
  `candidate_user_id` int(11) NOT NULL,
  `drive_drive_id` int(11) NOT NULL,
  `feed_back_feedback_id` int(11) DEFAULT NULL,
  `interviewer_user_id` int(11) NOT NULL,
  `recruiter_user_id` int(11) NOT NULL,
  PRIMARY KEY (`interview_id`),
  KEY `FK7g6vlnrnkrfwy2vh27px6p399` (`candidate_user_id`),
  KEY `FKesy8tg010of1uw4sh19xwbtow` (`interviewer_user_id`),
  KEY `FKjrdmbqhr0kyn3ssf6q57cdmr6` (`drive_drive_id`),
  KEY `FKnp48x1l27qpk745lice0xv0au` (`recruiter_user_id`),
  KEY `FKsy22cpen4rwlra7udl15g8ada` (`feed_back_feedback_id`),
  CONSTRAINT `FK7g6vlnrnkrfwy2vh27px6p399` FOREIGN KEY (`candidate_user_id`) REFERENCES `user_tbl` (`user_id`),
  CONSTRAINT `FKesy8tg010of1uw4sh19xwbtow` FOREIGN KEY (`interviewer_user_id`) REFERENCES `user_tbl` (`user_id`),
  CONSTRAINT `FKjrdmbqhr0kyn3ssf6q57cdmr6` FOREIGN KEY (`drive_drive_id`) REFERENCES `drive_tbl` (`drive_id`),
  CONSTRAINT `FKnp48x1l27qpk745lice0xv0au` FOREIGN KEY (`recruiter_user_id`) REFERENCES `user_tbl` (`user_id`),
  CONSTRAINT `FKsy22cpen4rwlra7udl15g8ada` FOREIGN KEY (`feed_back_feedback_id`) REFERENCES `feedback_tbl` (`feedback_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `interview`
--


--
-- Table structure for table `registration`
--

DROP TABLE IF EXISTS `registration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `registration` (
  `reg_id` int(11) NOT NULL AUTO_INCREMENT,
  `is_accepted` bit(1) DEFAULT NULL,
  `is_scheduled` bit(1) DEFAULT NULL,
  `timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `by_user_id` int(11) NOT NULL,
  `drive_drive_id` int(11) NOT NULL,
  PRIMARY KEY (`reg_id`),
  KEY `fk_drive_id_idx` (`drive_drive_id`),
  KEY `fk_user_id_idx` (`by_user_id`),
  CONSTRAINT `fk_drive_id` FOREIGN KEY (`drive_drive_id`) REFERENCES `drive_tbl` (`drive_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_user_id` FOREIGN KEY (`by_user_id`) REFERENCES `user_tbl` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `registration`
--


--
-- Table structure for table `user_tbl`
--

DROP TABLE IF EXISTS `user_tbl`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_tbl` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `date_of_birth` date DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `language_tags` varchar(255) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `phone_no` varchar(255) DEFAULT NULL,
  `register_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `role` int(11) NOT NULL,
  `skill_tags` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `email_UNIQUE` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_tbl`
--

/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-02-14 15:37:47
