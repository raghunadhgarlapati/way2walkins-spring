package com.way2walkins.util;

import java.sql.Date;

import com.way2walkins.entities.Drive;
import com.way2walkins.entities.User;

public class TestUtil {
	  
	 public static Drive getDriveObject(int driveId,int UserId) {
		  Drive drive=new Drive();
		  User user = getUserObject(UserId);
		  drive.setBy(user);
		  drive.setCity("raghu");
		  drive.setHeading("hello Drive");
		  drive.setWhen(new Date(10-10-2012));
		  return  drive;
	 }
	 
	 public static User getUserObject(int userId) {
		 User user = new User();
		 user.setUserId(userId);
		 return user;
	 }
}
