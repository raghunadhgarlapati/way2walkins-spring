package com.way2wakins.recruiter;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;
import com.way2walkins.dao.CandidateDAO;
import com.way2walkins.dao.DriveDao;
import com.way2walkins.dao.FeedBackDetailsDAO;
import com.way2walkins.dao.InterviewDAO;
import com.way2walkins.dao.RegistrationDAO;
import com.way2walkins.entities.Drive;
import com.way2walkins.entities.FeedBackDetails;
import com.way2walkins.entities.Interview;
import com.way2walkins.entities.Registration;
import com.way2walkins.entities.User;
import com.way2walkins.services.InterviewerServiceImpl;
import com.way2walkins.services.RecruiterServicesImp;
import com.way2walkins.util.ModelMapperUtil;
import com.way2walkins.util.RoleEnum;
import com.way2walkins.util.TestUtil;

/**
 * this class is used to test the all the api's referencing to recruiter service implementation
 * @author IMVIZAG
 *
 */
@RunWith(MockitoJUnitRunner.Silent.class)
@SpringBootTest
public class RecruiterCreateDriveTest {
   
	@Mock
	private DriveDao driveDao;
	
	@Mock
	private CandidateDAO candidateDAO;
	
	@Mock
	private InterviewDAO interviewDAO;
	
	@Mock
	private RegistrationDAO registrationDAO;
	
	@Mock
	private FeedBackDetailsDAO feedBackDetailsDAO;
	
	@Mock
	private ModelMapperUtil modelMapperUtil;
	
	@InjectMocks
	 private RecruiterServicesImp  recruiterServicesImp;
	
	@InjectMocks
    private InterviewerServiceImpl interviewerServiceImpl;
	
	
	/**
	 * this is used to test the api whether the drive is created or not
	 */
  @Test
  public void  getCreateDriveTest() {
	 Drive drive =  TestUtil.getDriveObject(2001,1002);
      when(candidateDAO.findById(1002)).thenReturn(Optional.of(drive.getBy()));
	  when(driveDao.save(drive)).thenReturn(drive);
//	  assertEquals(drive,recruiterServicesImp.createDrive(1002, drive));
	  assertTrue(recruiterServicesImp.createDrive(1002, drive));
	
	 }
  
  /**
   * this method is used to test  recruiter drives list with that particular drive Id
   */
  @Test
  public void getRecruterDrivesListTest()
  {   
	  
	  Drive drive= TestUtil.getDriveObject(2001,1002);
	  List<Drive> driveList=new ArrayList<Drive>();
	  driveList.add(drive);
	  when(driveDao.findAll()).thenReturn(driveList);
	  assertEquals(true, recruiterServicesImp.getRecruterDrivesList(1002).size()>0);
  }
	
  /**
   * this method is used to test the modify the drive 
   * by the recruiter
   * @throws Exception
   */
  @Test
  public void modifyDriveTest() throws Exception{
     Drive drive = TestUtil.getDriveObject(2001,1002);
     when(driveDao.save(drive)).thenReturn(drive);
	  assertTrue(recruiterServicesImp.modifyDrive(drive));
  }

  /**
   * this method is used to test that 
   * whether the drive is 
   */
	@Test
	public void scheduleDriveTest() {
	

	Drive drive = TestUtil.getDriveObject(2001,1001);
	//registration object creation and setting the values
	 Registration registration = new Registration();
	 registration.setBy(drive.getBy());
	 registration.setIs_scheduled(true);
	 drive.setRegistrations(Stream.of(registration).collect(Collectors.toList()));
	 
	 //finding the drives with drive id
	 when(driveDao.findById(2001)).thenReturn(Optional.of(drive));
	 User user = TestUtil.getUserObject(1001);
	when(candidateDAO.findById(1001)).thenReturn(Optional.of(user));
	User recruiter = TestUtil.getUserObject(1002);
	when(candidateDAO.findById(1002)).thenReturn(Optional.of(recruiter));
	//creating the interview object
	Interview interview = new Interview();
	interview.setCandidate(user);
	interview.setDrive(drive);
	interview.setRecruiter(recruiter);
	
    when(interviewDAO.save(interview)).thenReturn(interview);
	assertTrue(recruiterServicesImp.scheduleDrive(2001, 1001, interview, 1002));
}
  
	 /**
	  * this method is used to test set the feedback details 
	  */
	@Test
	public void setFeedBackDetailsTest() {
		
		Drive drive=TestUtil.getDriveObject(2001, 1002);
		when(driveDao.findById(2001)).thenReturn(Optional.of(drive));
		FeedBackDetails feedBackDetails=new FeedBackDetails();
		feedBackDetails.setCutoff_rating(5);
		feedBackDetails.setMax_rating(10);
		feedBackDetails.setSkill("good");
		feedBackDetails.setSkill_category(1);
		when(feedBackDetailsDAO.save(feedBackDetails)).thenReturn(feedBackDetails);
		drive.setFeedbackDetails(Stream.of(feedBackDetails).collect(Collectors.toList()));
		when(driveDao.save(drive)).thenReturn(drive);
		List<FeedBackDetails> setFeedbackDetailsList = new ArrayList<FeedBackDetails>();
		setFeedbackDetailsList.add(feedBackDetails);
		assertTrue(recruiterServicesImp.setFeedBackDetails(2001,setFeedbackDetailsList));
		}
	/**
	 * this method is used to test the reschedule drive  by the recruiter
	 */
	@Test
	public void reScheduleDriveTest() {
		
		Interview  interview = new Interview();
		when(interviewDAO.save(interview)).thenReturn(interview);
		assertTrue(recruiterServicesImp.reScheduleDrive(interview));
}
	/**
	 * this method is used to test the make drive complete  wether the drive is completed or not
	 */
	@Test
	public void makeDriveCompleteTest() { 


		Drive drive = TestUtil.getDriveObject(2001,1002);
		drive.setIs_conducted(true);
		when(driveDao.findById(2001)).thenReturn(Optional.of(drive));
		when(driveDao.save(drive)).thenReturn(drive);
		assertTrue(recruiterServicesImp.makeDriveComplete(2001));
		
	}
	/**
	 * this method is used to test the get all interviewers
	 */
     
	 @Test
	public  void getAllInterviewersTest() {
		 
		 
		 User interviewr=TestUtil.getUserObject(1003); 
		 List<User> interviewrs =new ArrayList<User>();
		 interviewrs.add(interviewr);
		 when(candidateDAO.findByRole(RoleEnum.INTERVIEWER.getValue())).thenReturn(interviewrs);
		assertTrue(recruiterServicesImp.getAllInterviewers().size()>0);
		
		
	}
	
	 /**
	  * this method is used to test  the get schedule object 
	  */

	 @Test
	public void getScheduleObjectTest() {
		 
			Interview interview = new Interview();
			when(interviewDAO.findByDriveIdandUserId(2001, 1003)).thenReturn(interview);
			assertNotNull(interview);
	}
    
	  /**
	   * this method is used to test the search by tag method in recruiter service implementation
	   * @throws Exception
	   */
	  @Test
	 public void searchByTagTest()throws Exception{
		 
		List<Drive> drives = new ArrayList<Drive>();
		when(driveDao.searchByRecrId("raghu",1002)).thenReturn(drives);
		assertTrue(recruiterServicesImp.searchByTag("raghu",1002).size()>=0);
}
}