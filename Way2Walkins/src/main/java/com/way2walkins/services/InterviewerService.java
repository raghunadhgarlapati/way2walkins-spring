package com.way2walkins.services;



import com.way2walkins.entities.FeedBack;
import com.way2walkins.entities.Interview;
import com.you_aspire.beens.InterviewerScheduledData;

public interface InterviewerService {
	
	public InterviewerScheduledData getAllInterviews(int interviewer_id) throws Exception;
	
	public boolean giveFeedBack(int driveId, int userId, int interviewerId, FeedBack feedBack) throws Exception;
//	
//	public boolean giveFeedBack(FeedBack feedback);
//	
//	public boolean getFeedBack(FeedBack feedback);

	public Interview getInterviewDetails(int interviewId) throws Exception;

	public InterviewerScheduledData searchByTag(String searchTag, int interviewerId) throws Exception;


	
}
