package com.way2walkins.services;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.way2walkins.configurations.UserPasswordEncription;
import com.way2walkins.dao.CandidateDAO;
import com.way2walkins.dao.DriveDao;
import com.way2walkins.dao.RegistrationDAO;
import com.way2walkins.entities.Drive;
import com.way2walkins.entities.LoginModel;
import com.way2walkins.entities.Registration;
import com.way2walkins.entities.User;
import com.way2walkins.util.ModelMapperUtil;
import com.way2walkins.util.StatusCodes;
import com.you_aspire.beens.CandidateTotalDrives;
import com.you_aspire.beens.DriveBasicDto;
import com.you_aspire.beens.UserBasicDto;

/**
 * this class is used to implement the services used by a candidates
 */
@Service
@Transactional
public class CandiateServicesImpl implements CandidateServices {
	
	@Autowired
	private CandidateDAO candidateDAO;
	@Autowired
	private DriveDao driveDao;

	@Autowired
	private RegistrationDAO registrationDao;

	@Autowired
	private ModelMapperUtil modelMapperUtil;

	@Override
	/**
	 * this method is used to verify the user registration password and with his
	 * password
	 */
	public UserBasicDto candidateLogin(LoginModel loginData) throws Exception {
		
	

		String candiateEmailid = loginData.getEmailId();
		User user = candidateDAO.findByEmail(candiateEmailid);
		UserBasicDto userBasic = null;
		if (user != null) {
			userBasic = modelMapperUtil.convertEntityToDto(user, UserBasicDto.class);
		} else {
			throw new Exception("Not a Registered user");
		}

		if (userBasic != null) {
			String inputPassword = UserPasswordEncription.getDecryptedPassword(user.getPassword());
			if (!inputPassword.equals(loginData.getPassword())) {
				return null;
			}
		}
		return userBasic;
	}

	
	/**
	 * this method is candidate (candidate,employer,recruiter,interviewer) into
	 * single user table
	 */
	@Override
	public StatusCodes candidateRegistration(User user) throws Exception {
		User result = null;
		result = candidateDAO.save(user);
		if (result != null) {
			return StatusCodes.OK;
		}

		return StatusCodes.ERROR;
	}
	

	/**
	 * this method gives the candidate drives in the
	 */
	@Override
	public CandidateTotalDrives getCandidateDrives(int candidateId) throws Exception {
		
		if(candidateId < 1000)
		{
			return null;
		}
		Iterable<Drive> drives = driveDao.findAll();
		CandidateTotalDrives candidateTotalDrives = fillGetCandidateDrives(candidateId, drives);
		return candidateTotalDrives;

	}

	/**
	 * this method is implemented for re usability
	 * 
	 * @param candidateId
	 * @param drives
	 * @return
	 * @throws Exception
	 */

	public CandidateTotalDrives fillGetCandidateDrives(int candidateId, Iterable<Drive> drives) throws Exception {

		List<DriveBasicDto> sugestedDrives = new ArrayList<DriveBasicDto>();
		List<DriveBasicDto> registeredDrives = new ArrayList<DriveBasicDto>();

		for (Drive drive : drives) {

			List<Registration> registrations = drive.getRegistrations();
			int count = 0;
			for (Registration registration : registrations) {
				if (registration.getBy().getUserId() == candidateId) {
					count++;
					break;
				}
			}

			DriveBasicDto driveBasicDto = modelMapperUtil.convertEntityToDto(drive, DriveBasicDto.class);
			if (count > 0) {

				registeredDrives.add(driveBasicDto);
			} else {
				sugestedDrives.add(driveBasicDto);
			}
		}

		CandidateTotalDrives candidateTotalDrives = new CandidateTotalDrives();
		candidateTotalDrives.setRegiteredDrives(registeredDrives);
		candidateTotalDrives.setSuggestedDrives(sugestedDrives);
		return candidateTotalDrives;
	}

	/**
	 * this method is used for candidate registration for particular drive
	 */

	@Override
	public boolean userRegisterToDrive(int userId, int driveId) throws Exception {
		
		boolean status=false;
		if((userId)<1000&&(driveId)<2000)
		{
			return status;
		}
		else
		{
			Drive drive = driveDao.findById(driveId).get();
			User user = candidateDAO.findById(userId).get();
			if (drive != null && user != null) {
				Registration registration = new Registration();
				registration.setDrive(drive);
				registration.setBy(user);
				Registration registrationstatus = registrationDao.save(registration);

				if (registrationstatus != null) {
					status= true;
				}
			}
		}
		return status;
		
	
	}

	/**
	 * used to get the drive details return Drive
	 */
	@Override
	public Drive getDrive(int driveId, int candidateId) throws Exception {

		Drive drive = driveDao.findById(driveId).get();

		if (drive != null) {
			boolean isRegistered = drive.getRegistrations().stream()
					.anyMatch(it -> (it.getBy().getUserId() == candidateId));
			drive.setIsRegistered(isRegistered);
		}
		return drive;
	}

	/**
	 * used to get the details of the particular string
	 */
	@Override
	public CandidateTotalDrives searchByTag(String searchTag, int candidateId) throws Exception {
		List<Drive> drives = driveDao.findByTag(searchTag);
		CandidateTotalDrives candidateTotalDrives = fillGetCandidateDrives(candidateId, drives);
		return candidateTotalDrives;
	}

	/**
	 * this is used to get the user info
	 */

	@Override
	public User GetUserInfo(int userId) throws Exception {
		if(userId<1000)
		{
			return null;
		}

		User user = candidateDAO.findById(userId).get();
		return user;

	}
	
	@Override
	public URL uploadImage(MultipartFile imageFile) throws IOException {
		
		
		final String imagePath = "images/"; //path
        String[] imageNames = (imageFile.getOriginalFilename().split("\\\\"));
        String imageName = imagePath + imageNames[imageNames.length-1];
        @SuppressWarnings("resource")
		FileOutputStream output = new FileOutputStream(imageName);
		output.write(imageFile.getBytes());
	    return new FileSystemResource(new File(imageName)).getURL();
	    
		
	}


	 /**
	  * this method is used to verify the email whether it is existed or not
	  */

	@Override
	public boolean isExisted(String email) {
		 
		User user = candidateDAO.findByEmail(email);
		return user != null;
		
	}

 
	 /**
	  * this method is used to modify the details of the user
	  * 
	  */
	@Override
	public User editUserInfo(User user) throws Exception {
		 
		return candidateDAO.save(user);
	
	}

}
