package com.way2walkins.services;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.way2walkins.dao.DriveDao;
import com.way2walkins.dao.EmployeeDao;
import com.way2walkins.dao.InterviewDAO;
import com.way2walkins.entities.Drive;
import com.way2walkins.entities.Interview;
import com.way2walkins.entities.User;
import com.you_aspire.beens.DriveInterviews;
import com.way2walkins.util.ModelMapperUtil;
import com.way2walkins.util.RoleEnum;
import com.you_aspire.beens.DriveBasicDto;
import com.you_aspire.beens.InterviewBasicDto;
import com.you_aspire.beens.UserBasicDto;

/**
 * this method gives the drives which are created by this employer
 * 
 * @author IMVIZAG
 *
 */
@Service
public class EmployerServiceImpl implements EmployerService {
	
	@Autowired
	private InterviewDAO interviewDAO;
	
	@Autowired
	private DriveDao driveDao;
	
	@Autowired
	private ModelMapperUtil modelMapperUtil;
	
	@Autowired
	private EmployeeDao  employeeDao;

	@Override
	public List<DriveBasicDto> getDrives(int employerId) {
		
		Iterable<Drive> drivesList = driveDao.findAll();
	    return fillEmployerDrives(drivesList, employerId);
	}

	/**
	 * this method returns the all the candidate feedBack information
	 */
	@Override

	public DriveInterviews getInterviewsList(int driveId) {
		
		Drive drive = driveDao.findById(driveId).get();
		List<Interview> interviews = interviewDAO.findByDrive(drive);
		
		List<InterviewBasicDto> interviewsList = interviews.stream()
				.map((interview) -> modelMapperUtil.convertEntityToDto(interview, InterviewBasicDto.class))
				.collect(Collectors.toList());
      
		List<InterviewBasicDto> completedInterviews = new ArrayList<InterviewBasicDto>();
		List<InterviewBasicDto> scheduledInterviews = new ArrayList<InterviewBasicDto>();
		
		for(InterviewBasicDto interview:interviewsList) {
			if(interview.isIs_conducted()) {
				completedInterviews.add(interview);
			}
			else {
				scheduledInterviews.add(interview);
			}
		}
		
		DriveInterviews driveInterviews = new DriveInterviews();
		driveInterviews.setCompletedInterviews(completedInterviews);
		driveInterviews.setScheduledInterviews(scheduledInterviews);
		return driveInterviews;
		
	}

	@Override
	public List<UserBasicDto> getAllEmployers() throws Exception {
		
		List<User> employers = employeeDao.findByRole(RoleEnum.EMPLOYER.getValue());
		List<UserBasicDto> employerBasic = new ArrayList<UserBasicDto>();
		for (User employer : employers) {
			UserBasicDto userBasic = modelMapperUtil.convertEntityToDto(employer, UserBasicDto.class);
			employerBasic.add(userBasic);

		}
		
		return employerBasic;
	}
	
	
	
	@Override
	
	public List<DriveBasicDto> searchByTag(String searchTag, int employerId) throws Exception {
		
		List<Drive> drivesList = driveDao.findByTag(searchTag);
		return fillEmployerDrives(drivesList, employerId);
 		
	}
	
	
	private List<DriveBasicDto> fillEmployerDrives(Iterable<Drive> drivesList,int employerId){
		
		List<DriveBasicDto> drives = new ArrayList<DriveBasicDto>();

		for (Drive drive : drivesList) {
			if (drive.getEmployers().stream().anyMatch(it -> (it.getUserId() == employerId))) {
				DriveBasicDto driveBasic = modelMapperUtil.convertEntityToDto(drive, DriveBasicDto.class);
				drives.add(driveBasic);
			}
		}
		return drives;
	}
}
