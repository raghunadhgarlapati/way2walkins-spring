package com.way2walkins.services;



import java.io.IOException;
import java.net.URL;

import org.springframework.web.multipart.MultipartFile;

import com.way2walkins.entities.Drive;
import com.way2walkins.entities.LoginModel;
import com.way2walkins.entities.User;
import com.way2walkins.util.StatusCodes;
import com.you_aspire.beens.CandidateTotalDrives;
import com.you_aspire.beens.UserBasicDto;

public interface CandidateServices {

	public UserBasicDto candidateLogin(LoginModel loginModel) throws Exception;

	public StatusCodes candidateRegistration(User user) throws Exception;

	public CandidateTotalDrives getCandidateDrives(int candidateId) throws Exception;

	public boolean userRegisterToDrive(int userId, int driveId) throws Exception;

	public Drive getDrive(int driveId,int candidateId)throws Exception;
	
	public User GetUserInfo(int userId) throws Exception;
	

	public URL uploadImage(MultipartFile imageFile) throws IOException;

	public CandidateTotalDrives searchByTag(String searchTag, int candidateId) throws Exception;

	public boolean isExisted(String email);


	public User editUserInfo(User user) throws Exception;

}
