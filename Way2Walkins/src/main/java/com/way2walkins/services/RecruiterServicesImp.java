package com.way2walkins.services;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.way2walkins.dao.CandidateDAO;
import com.way2walkins.dao.DriveDao;
import com.way2walkins.dao.InterviewDAO;
import com.way2walkins.entities.Drive;
import com.way2walkins.entities.FeedBackDetails;
import com.way2walkins.entities.Interview;
import com.way2walkins.entities.Registration;
import com.way2walkins.entities.User;
import com.way2walkins.util.ModelMapperUtil;
import com.way2walkins.util.RoleEnum;
import com.you_aspire.beens.DriveBasicDto;
import com.you_aspire.beens.UserBasicDto;

@Service
@Transactional
public class RecruiterServicesImp implements RecruiterServices {

	@Autowired
	private DriveDao driveDao;

	@Autowired
	private CandidateDAO userDao;

	@Autowired
	private InterviewDAO interviewDAO;

	@Autowired
	private ModelMapperUtil modelMapperUtil;

	/**
	 * this method is used to display the list of drives for of a recruiter 
	 * return type is List<Drives>
	 * 
	 */
	@Override
	public List<DriveBasicDto> getRecruterDrivesList(int recruterId) {
		
		if (recruterId < 1000) {
			return null;
		} 

		Iterable<Drive> driveList = driveDao.findAll();
		List<DriveBasicDto> drives = new ArrayList<DriveBasicDto>();

		for (Drive drive : driveList) {
			if (drive.getBy().getUserId() == recruterId) {
				DriveBasicDto driveBasic = modelMapperUtil.convertEntityToDto(drive, DriveBasicDto.class);

				drives.add(driveBasic);
			}
		}
		return drives;
	}

	/**
	 * this method is used to create a drive by a recruiter return the status of
	 * boolean type
	 */
	@Override
	
	public boolean createDrive(int recruterId, Drive drive) {
		
		Drive driveStatus = null;
		try {
			User recruiter = userDao.findById(recruterId).get();
			drive.setBy(recruiter);
			driveStatus = driveDao.save(drive);
		} catch (Exception e) {
             return false;
		}

		if (driveStatus != null) {
			return true;
		}

		return false;
	}

	/**
	 * this method is used to modify the particular drive created by the recruiter
	 */
	@Override
	public boolean modifyDrive(Drive drive) throws Exception {
		
		Drive driveStatus = driveDao.save(drive);
		return driveStatus != null;

	}

	/**
	 * this method is used to schedule the drive by a recruiter it return status
	 */

	@Override
	public boolean scheduleDrive(int driveId, int candidateId, Interview interview, int recruiterId) {

		boolean status = false;
		Drive drive = driveDao.findById(driveId).get();
		List<Registration> registrationList = drive.getRegistrations();
		for (Registration registration : registrationList) {
			if (registration.getBy().getUserId() == candidateId) {
				registration.setIs_scheduled(true);
				status = true;
			}
		}

		User user = userDao.findById(candidateId).get();
		User recruiter = userDao.findById(recruiterId).get();
		drive.setDriveId(driveId);

		interview.setCandidate(user);

		interview.setDrive(drive);
		interview.setRecruiter(recruiter);

		Interview interviewStatus = interviewDAO.save(interview);
		boolean is_interviewSaved = false;
		if(interviewStatus != null) {
			is_interviewSaved = true;
		}
		return status && is_interviewSaved;

	}

	/**
	 * this method is used to set the list of feedback details of a drive by the
	 * recruiter
	 */
	@Override
	public boolean setFeedBackDetails(int driveId, List<FeedBackDetails> feedBackDetails) {
		
		boolean status = false;
		Drive drive = driveDao.findById(driveId).get();

		drive.setFeedbackDetails(feedBackDetails);

		Drive driveStatus = driveDao.save(drive);
		if (driveStatus != null)
			status = true;
		return status;
	}

	/**
	 * this method is used to reschedule the interview of a particular candidate who
	 * was already scheduled return status
	 */

	@Override
	public boolean reScheduleDrive(Interview interview) {
		
		Interview interviewStatus = interviewDAO.save(interview);
		return interviewStatus != null;

	}

	/**
	 * this method returns all the interviewers
	 */
	@Override
	public List<UserBasicDto> getAllInterviewers() {
		List<User> interviewrs = userDao.findByRole(RoleEnum.INTERVIEWER.getValue());
		List<UserBasicDto> interviewersBasic = new ArrayList<UserBasicDto>();
		for (User interviewer : interviewrs) {
			UserBasicDto userBasic = modelMapperUtil.convertEntityToDto(interviewer, UserBasicDto.class);
			interviewersBasic.add(userBasic);

		}
		return interviewersBasic;
	}

	/**
	 * this method is used to get the interview object whose drive is scheduled
	 */
	@Override
	public Interview getScheduleObject(int driveId, int candidateId) throws Exception {
		
		Interview interview = interviewDAO.findByDriveIdandUserId(driveId, candidateId);
		return interview;

	}

	/**
	 * this method is used to make drive is completed
	 */
	@Override
	public boolean makeDriveComplete(int driveId) {

		boolean status = false;
		Drive drive = driveDao.findById(driveId).get();
		drive.setIs_conducted(true);
		Drive statusDrived = driveDao.save(drive);
		if (statusDrived != null) {
			status = true;
		}

		return status;

	}

	/**
	 * this method is used to search the tag and give those related drives by using
	 * tag search
	 */

	@Override
	public List<DriveBasicDto> searchByTag(String searchTag, int recruiterId) throws Exception {
		
		List<Drive> drives = driveDao.searchByRecrId(searchTag, recruiterId);
		List<DriveBasicDto> driveBasicsList = (drives.stream()
				.map(drive -> modelMapperUtil.convertEntityToDto(drive, DriveBasicDto.class))
				.collect(Collectors.toList()));
		return driveBasicsList;

	}

	/**
	 * this method is used to get the all the recruiters
	 */
	@Override
	public List<UserBasicDto> getAllRecruiters() throws Exception {
		
		List<User> recruiters = userDao.findByRole(RoleEnum.RECRUITER.getValue());
		List<UserBasicDto> recruitersBasic = new ArrayList<UserBasicDto>();
		for (User recruiter : recruiters) {
			UserBasicDto userBasic = modelMapperUtil.convertEntityToDto(recruiter, UserBasicDto.class);
			recruitersBasic.add(userBasic);

		}
		return recruitersBasic;

	}

}
