package com.way2walkins.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.stream.Collectors;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import com.way2walkins.dao.CandidateDAO;
import com.way2walkins.dao.DriveDao;
import com.way2walkins.dao.FeedBackDAO;
import com.way2walkins.dao.InterviewDAO;
import com.way2walkins.dao.RegistrationDAO;
import com.way2walkins.entities.Drive;
import com.way2walkins.entities.FeedBack;
import com.way2walkins.entities.FeedBackDetails;
import com.way2walkins.entities.Interview;
import com.way2walkins.entities.Registration;
import com.way2walkins.entities.User;
import com.way2walkins.util.ModelMapperUtil;
import com.you_aspire.beens.InterviewBasicDto;
import com.you_aspire.beens.InterviewDataForDrive;
import com.you_aspire.beens.InterviewerScheduledData;

/**
 * This class provides implements of InterviewerService.
 * 
 * @author IMVIZAG
 *
 */
@Service
@Transactional
public class InterviewerServiceImpl implements InterviewerService {

	@Autowired
	private FeedBackDAO feedbackDAO;

	@Autowired
	private DriveDao driveDao;

	@Autowired
	private InterviewDAO interviewDAO;

	@Autowired
	private CandidateDAO userDao;

	@Autowired
	private RegistrationDAO registrationDAO;
	@Autowired
	private ModelMapperUtil modelMapperUtil;

	
	/**
	 * this returns the total interviews of particular interviewer
	 */
	public InterviewerScheduledData getAllInterviews(int interviewer_id) {
		
		User interviewer = userDao.findById(interviewer_id).get();
		List<Interview> interviewList = interviewDAO.findByInterviewer(interviewer);
		InterviewerScheduledData interviewerScheduledData = getScheduledAndCompletedInterviews(interviewer_id, interviewList);
		return interviewerScheduledData;
		
	}
	
	
	/**
	 * this method is used to  get the Scheduled And UnScheduled drives  of the interviewer 
	 * this method is used for reusability of the code
	 * @param interviewer_id
	 * @return
	 */
	
	public  InterviewerScheduledData getScheduledAndCompletedInterviews(int interviewer_id,Iterable<Interview> interviewList)
	{
		
		
		List<Interview> interviews = new ArrayList<Interview>();

		for (Interview interview : interviewList) {
			interviews.add(interview);
		}

		// List<InterviewDataForDrive> driveInterviews = new
		// ArrayList<InterviewDataForDrive>();
		Map<Integer, List<InterviewBasicDto>> drivesMap = new TreeMap<Integer, List<InterviewBasicDto>>();

		for (int i = 0; i < interviews.size(); i++) {
			int driveId = interviews.get(i).getDrive().getDriveId();
			Interview interview = interviews.get(i);

			List<InterviewBasicDto> interviewBasicList = null;
//			for (int j = 0; j < interviews.size(); j++) {

				if (driveId == interviews.get(i).getDrive().getDriveId()) {
					interviewBasicList = drivesMap.get(driveId);

					InterviewBasicDto interviewDto = modelMapperUtil.convertEntityToDto(interview,
							InterviewBasicDto.class);

					if (interviewBasicList != null) {

						interviewBasicList.add(interviewDto);
					} else {
						interviewBasicList = new ArrayList<InterviewBasicDto>();
						interviewBasicList.add(interviewDto);
					}

				} else {

				}
			//}

			drivesMap.put(driveId, interviewBasicList);
		}

		Set<Integer> keys = drivesMap.keySet();

		List<InterviewDataForDrive> interviewDataForDrives = new ArrayList<InterviewDataForDrive>();
		for (Integer key : keys) {
			InterviewDataForDrive interviewDataForDrive = new InterviewDataForDrive();
			Drive drive = driveDao.findById(key).get();
			interviewDataForDrive.setDrive(drive);
			interviewDataForDrive.setInterviews(drivesMap.get(key));
			interviewDataForDrives.add(interviewDataForDrive);
		}

		List<InterviewDataForDrive> completedInterviews = new ArrayList<InterviewDataForDrive>();
		List<InterviewDataForDrive> scheduledInterviews = new ArrayList<InterviewDataForDrive>();

		for (InterviewDataForDrive interviewData : interviewDataForDrives) {
			boolean notCompleted = interviewData.getInterviews().stream()
					.anyMatch(it -> (it.isIs_conducted() == false));
			if (notCompleted) {
				scheduledInterviews.add(interviewData);
			} else {
				completedInterviews.add(interviewData);
			}
		}

		InterviewerScheduledData interviewerScheduledData = new InterviewerScheduledData();

		interviewerScheduledData.setCompletedInterviews(completedInterviews);
		interviewerScheduledData.setScheduledInterviews(scheduledInterviews);
		return interviewerScheduledData;

	}

	/**
	 * creating the interviewData object
	 * 
	 * 
	 */
	/**
	 * this method
	 * 
	 * @param driveId
	 * @param userId
	 * @param interviewerId
	 * @param feedBack
	 * @return
	 */

	@Override
	public boolean giveFeedBack(int driveId, int userId, int interviewerId, FeedBack feedBack) {
		boolean status = false;

		User interviewer = userDao.findById(interviewerId).get();
		Iterable<Interview> interviewList = interviewDAO.findByInterviewer(interviewer);
		Interview perticullerInterview = null;
		for (Interview interview : interviewList) {
			if (interview.getDrive().getDriveId() == driveId && interview.getCandidate().getUserId() == userId) {
				perticullerInterview = interview;
				break;
			}
		}

		if (perticullerInterview != null) {
			status = true;

			List<FeedBackDetails> feedBackDetailsList = feedBack.getDrill_down_feedback();
			for (FeedBackDetails feedBackDetails : feedBackDetailsList) {
				feedBackDetails.setFeedBack(feedBack);
			}
			feedBack = feedbackDAO.save(feedBack);
			perticullerInterview.setFeedBack(feedBack);
			perticullerInterview.setIs_conducted(true);

			// setting the is_accepted filed in the registration table
			Drive drive = driveDao.findById(driveId).get();
			List<Registration> registrations = drive.getRegistrations().stream()
					.filter(it -> (it.getBy().getUserId() == userId)).collect(Collectors.toList());

			if (registrations.size() > 0) {
				Registration registration = registrations.get(0);
				registration.setIs_accepted(true);
				registrationDAO.save(registration);
			}

			interviewDAO.save(perticullerInterview);

		}
		return status;
	}

	/**
	 * this method is used to get the interview details of the particular interview
	 * id
	 */
	@Override
	public Interview getInterviewDetails(int interviewId) {
		Interview interview = interviewDAO.findById(interviewId).get();
		return interview;
	}
	
	
   /**
    * this method is used to get hte interview schedule data by searching the tag
    */
	@Override
	
	public InterviewerScheduledData searchByTag(String searchTag, int interviewerId) throws Exception {
		
		List<Interview> searchInterviews=new ArrayList<Interview>();
		Iterable<Interview> InterviewList = interviewDAO.findAll();
		List<Drive> driveList=(driveDao.findByTag(searchTag));
		for(Drive driveVariable: driveList)
		{
			for(Interview interviewVariable:InterviewList)
			{
				if(driveVariable.getDriveId()==interviewVariable.getDrive().getDriveId())
				{
					searchInterviews.add(interviewVariable);
				}
			}
		}
		
		
		InterviewerScheduledData interviewerScheduledData = getScheduledAndCompletedInterviews(interviewerId,searchInterviews);
		return interviewerScheduledData;
	
	}
}
