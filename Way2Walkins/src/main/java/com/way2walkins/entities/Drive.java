
package com.way2walkins.entities;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import org.hibernate.annotations.GenericGenerator;
/**
 * This class implements the drive registration details. 
 * 
 * @author yo_Aspire
 *
 */
@Entity
@Table(name = "Drive_tbl")

public class Drive {
	  
	//Fields and setters,getters  are initialized for drive table.
	
	@Id
	@GenericGenerator(name = "IdGen", strategy = "com.way2walkins.util.IdGenerator")
	@GeneratedValue(generator = "IdGen")
	@Column(name = "drive_id")
	private int driveId;

	@ManyToOne
	@JoinColumn(name = "created_by")
	private User by;

	@Column(name = "heading")
	private String heading;

	@Column(name = "sub_heading")
	private String sub_heading;

	@Column(name = "description")
	private String description;

	@Column(name = "eligibility")
	private String eligibility;

	@Column(name = "more_information")
	private String more_information;

	@Column(name = "exactly")
	private String exactly;

	@Column(name = "image")
	private String image;

	@Column(name = "url")
	private String url;

	@Column(name = "phone")
	private String phone;

	@Column(name = "city")
	private String city;

	@Column(name = "where_created")
	private String where;

	@Column(name = "when_created")
	private Date when;

	@Column(name = "tags")
	private String tags;

	@Column(name = "register_before")
	private Date register_before;

	@Column(name = "is_deleted")
	private boolean is_deleted;

	@Column(name = "is_conducted")
	private boolean is_conducted;

	@Column(name = "time_stamp")
	private Date time_stamp;
	
	@Transient
	private boolean isRegistered;
	
	public boolean getIsRegistered() {
		return isRegistered;
	}

	public void setIsRegistered(boolean isRegistered) {
		this.isRegistered = isRegistered;
	}

	 
	
	
	@ManyToMany(targetEntity = FeedBackDetails.class, cascade = { CascadeType.ALL })
	@JoinTable(name = "drive_feed_back_details", joinColumns = { @JoinColumn(name = "driveId") }, inverseJoinColumns = {
			@JoinColumn(name = "feedback_details_id") })
	private List<FeedBackDetails> feedbackDetails = new ArrayList<FeedBackDetails>();

	@OneToMany(mappedBy = "drive")
	private List<Registration> registrations = new ArrayList<Registration>();
 
	@ManyToMany(targetEntity = User.class)
	@JoinTable(name = "drive_interviewer_tbl", joinColumns = { @JoinColumn(name = "drive_id") }, inverseJoinColumns = {
			@JoinColumn(name = "interviewer_id") })

	private List<User> interviewers = new ArrayList<User>();

	@ManyToMany(targetEntity = User.class)
	@JoinTable(name = "drive_employer_tbl", joinColumns = { @JoinColumn(name = "drive_id") }, inverseJoinColumns = {
			@JoinColumn(name = "employer_id") })

	private List<User> employers = new ArrayList<User>();

	public Date getTime_stamp() {
		return time_stamp;
	}

	public void setTime_stamp(Date time_stamp) {
		this.time_stamp = time_stamp;
	}

	public List<User> getEmployers() {
		return employers;
	}

	public void setEmployers(List<User> employers) {
		this.employers = employers;
	}

	public User getBy() {
		return by;
	}

	public void setBy(User by) {
		this.by = by;
	}

	public List<FeedBackDetails> getFeedbackDetails() {
		return feedbackDetails;
	}

	public void setFeedbackDetails(List<FeedBackDetails> feedbackDetails) {
		this.feedbackDetails = feedbackDetails;
	}

	public List<Registration> getRegistrations() {
		return registrations;
	}

	public void setRegistrations(List<Registration> registrations) {
		this.registrations = registrations;
	}

	public List<User> getInterviewers() {
		return interviewers;
	}

	public void setInterviewers(List<User> interviewers) {
		this.interviewers = interviewers;
	}

	public int getDriveId() {
		return driveId;
	}

	public void setDriveId(int driveId) {
		this.driveId = driveId;
	}

	public String getHeading() {
		return heading;
	}

	public void setHeading(String heading) {
		this.heading = heading;
	}

	public String getSub_heading() {
		return sub_heading;
	}

	public void setSub_heading(String sub_heading) {
		this.sub_heading = sub_heading;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getEligibility() {
		return eligibility;
	}

	public void setEligibility(String eligibility) {
		this.eligibility = eligibility;
	}

	public String getMore_information() {
		return more_information;
	}

	public void setMore_information(String more_information) {
		this.more_information = more_information;
	}

	public String getExactly() {
		return exactly;
	}

	public void setExactly(String exactly) {
		this.exactly = exactly;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getWhere() {
		return where;
	}

	public void setWhere(String where) {
		this.where = where;
	}

	public Date getWhen() {
		return when;
	}

	public void setWhen(Date when) {
		this.when = when;
	}

	public String getTags() {
		return tags;
	}

	public void setTags(String tags) {
		this.tags = tags;
	}

	public Date getRegister_before() {
		return register_before;
	}

	public void setRegister_before(Date register_before) {
		this.register_before = register_before;
	}

	public boolean getIs_deleted() {
		return is_deleted;
	}

	public void setIs_deleted(boolean is_deleted) {
		this.is_deleted = is_deleted;
	}

	public boolean getIs_conducted() {
		return is_conducted;
	}

	public void setIs_conducted(boolean is_conducted) {
		this.is_conducted = is_conducted;
	}

}
