package com.way2walkins.entities;
/**
 * This class displays the login form details.
 * @author yo_Aspire
 *
 */
public class LoginModel {
	
	private String emailId;
	private String password;
	//setters and getters generated
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
}
