package com.way2walkins.entities;



import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
/**
 * This class implements the interview details.
 * @author IMVIZAG
 *
 */
@Entity
@Table(name="interview")
public class Interview {
	//Interview table fields and setters and getters are initialized.
	@Id
	@GenericGenerator(name = "IdGen" ,strategy = "com.way2walkins.util.IdGenerator" )
	@GeneratedValue(generator = "IdGen")
	@Column(name = "interview_id")
	private int interviewId;
	//Adding relations for interview table.
	//many interviews for a single drive relation is added.
	@ManyToOne
	private Drive drive;
	//Many candidates to a single interview.
	@ManyToOne
	private User candidate;
	//Many recruiters can assign a interviews.
	@ManyToOne
	private User recruiter;

	//one to one mapping for feedback to interview
	@OneToOne
	private FeedBack feedBack;
	
	@ManyToOne
	private User interviewer;

	@Column
	private Date when_interviewed;
 
	 @Column
	private String exactly;

	 @Column
	private boolean is_conducted;

	

	public int getInterviewId() {
		return interviewId;
	}

	public void setInterviewId(int interviewId) {
		this.interviewId = interviewId;
	}

	public Drive getDrive() {
		return drive;
	}

	public void setDrive(Drive drive) {
		this.drive = drive;
	}

	public User getCandidate() {
		return candidate;
	}

	public void setCandidate(User candidate) {
		this.candidate = candidate;
	}

	public User getRecruiter() {
		return recruiter;
	}

	public void setRecruiter(User recruiter) {
		this.recruiter = recruiter;
	}

	public User getInterviewer() {
		return interviewer;
	}

	public void setInterviewer(User interviewer) {
		this.interviewer = interviewer;
	}



	public Date getWhen_interviewed() {
		return when_interviewed;
	}

	public void setWhen_interviewed(Date when_interviewed) {
		this.when_interviewed = when_interviewed;
	}

	public String getExactly() {
		return exactly;
	}

	public void setExactly(String exactly) {
		this.exactly = exactly;
	}

	public boolean isIs_conducted() {
		return is_conducted;
	}

	public void setIs_conducted(boolean is_conducted) {
		this.is_conducted = is_conducted;
	}

	public FeedBack getFeedBack() { 
		return feedBack;
	}

	public void setFeedBack(FeedBack feedBack) {
		this.feedBack = feedBack;
	}

}
