package com.way2walkins.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.way2walkins.entities.User;

public interface UserRepository extends JpaRepository<User,Integer>{

}
