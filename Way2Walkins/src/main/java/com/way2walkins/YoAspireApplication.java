
package com.way2walkins;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.way2walkins.services.ImageStorageService;

@SpringBootApplication
public class YoAspireApplication {
    
	@Autowired
	ImageStorageService imageStorageService;
	
	
	public static void main(String[] args) {
		SpringApplication.run(YoAspireApplication.class, args);
	}
	
	@Bean
	public ModelMapper getModelMapper() {
		return new ModelMapper();
	} 
}

