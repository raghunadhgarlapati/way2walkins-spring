package com.way2walkins.util;



import java.util.Random;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;
import io.jsonwebtoken.Claims;

@Component
public class EmailVerfication {
	
	@Autowired
   public JavaMailSender sender;
	
	
	public static int generateRandomNumber() {
		Random rand = new Random();
		int number = rand.nextInt(10000);
		while(true) {
			if(number < 1000) {
				generateRandomNumber();
			}else {
				return number;
			}	
		}
	}
	

	 public  boolean sendMail(String email,int  randomNo)
	 {
		 
	        MimeMessage message = sender.createMimeMessage();
	        MimeMessageHelper helper = new MimeMessageHelper(message);

	        try {
	        	
	            helper.setTo(email);
	            helper.setSubject("Verfication For Way2Walkins " );
	            helper.setText("Hello this is the verfication otp.."+randomNo);
	         
	        } catch (MessagingException e) {
	            e.printStackTrace();
	            return true;
	        }
	        sender.send(message);
	        return true;
	    }
	 
//	 public  boolean sendMail(String email,String   sucess)
//	 {
//		 
//	        MimeMessage message = sender.createMimeMessage();
//	        MimeMessageHelper helper = new MimeMessageHelper(message);
//
//	        try {
//	        	
//	            helper.setTo(email);
//	            helper.setSubject("your email has been varified sucessfully " ); 
//	            helper.setText("String");
//	         
//	        } catch (MessagingException e) {
//	            e.printStackTrace();
//	            return true;
//	        }
//	        sender.send(message);
//	        return true;
//	    }
//	 
		
		public static boolean isValidOtp(String token,int otp) {
			boolean result = false;
			Claims claims = JwtImpl.decodeJWT(token);
			if (claims != null) {
				int  tokenOtp = Integer.parseInt(claims.getId());
						if (tokenOtp==otp)
					result = true;
			}
			return result;
		}
	
}
