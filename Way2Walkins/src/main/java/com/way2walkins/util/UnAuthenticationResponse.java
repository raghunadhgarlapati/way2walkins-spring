package com.way2walkins.util;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
/**
 * this class returns the common code response object
 * @author IMVIZAG
 *
 */
public class UnAuthenticationResponse {

/**
 * this method returns the UnAutherized resoponse 
 * @return
 */
  public static ResponseEntity<ResponseType> getResponse(String token) {
	  ResponseType responseType = new ResponseType();
	  responseType.setResponse("Un autherized user");
	  responseType.setStatusCode(StatusCodes.NOT_AUTH.getValue());
	  responseType.setToken(token);
	  return new ResponseEntity<ResponseType>(responseType,HttpStatus.UNAUTHORIZED);
  }
}
