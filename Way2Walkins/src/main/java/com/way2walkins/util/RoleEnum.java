package com.way2walkins.util;

/**
 * this enum, is used for identifying the user as candidate ,interviewer,RECRUITER,EMPLOYER
 * 
 * @author IMVIZAG
 *
 */
public enum RoleEnum {
	//roles are initialized
	CANDIDATE(1), EMPLOYER(2), INTERVIEWER(3), RECRUITER(4);
	private int value;

	public int getValue() {
		return value;
	}

	private RoleEnum(int value) {
       this.value = value;
	}
}
