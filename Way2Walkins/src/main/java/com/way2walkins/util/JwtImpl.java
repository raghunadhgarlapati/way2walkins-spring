package com.way2walkins.util;

import java.security.Key;
import java.util.Date;

import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

/**
 * this class used to generate the tokens for the particular responses of their
 * api's
 * 
 * @author IMVIZAG
 *
 */
public class JwtImpl {

	// The secret key. This should be in a property file NOT under source
	// control and not hard coded in real life. We're putting it here for
	// simplicity.
	private static String SECRET_KEY = "oeRaYY7Wo24sDqKSX3IM9ASGmdGPmkTd9jo1QTy4b7P9Ze5_9hKolVX8xNrQDcNRfVEdTZNOuOyqEGhXEbdJI";

	// Sample method to construct a JWT
	public static String createJWT(Integer userId) {
		long nowMillis = System.currentTimeMillis();
		Date now = new Date(nowMillis);
		long expiryMilliseconds = 60*60*1000;

		SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;

		byte[] apiKeySecretBytes = DatatypeConverter.parseBase64Binary(SECRET_KEY);
		Key signingKey = new SecretKeySpec(apiKeySecretBytes, signatureAlgorithm.getJcaName());

		// Let's set the JWT Claims
		JwtBuilder builder = Jwts.builder().setId(userId.toString())
				.setIssuedAt(now).signWith(signatureAlgorithm,
				signingKey);


        //if it has been specified, let's add the expiration
        if (expiryMilliseconds >= 0) {
            long expMillis = nowMillis + expiryMilliseconds;
            Date exp = new Date(expMillis);
            builder.setExpiration(exp);
        }

		// Builds the JWT and serializes it to a compact, URL-safe string
		return builder.compact();
	}
	
	
	// Sample method to construct a JWT
		public static String registrationToken(Integer otp) {
			long nowMillis = System.currentTimeMillis();
			Date now = new Date(nowMillis);
			long expiryMilliseconds = 60*60*1000;

			SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;

			byte[] apiKeySecretBytes = DatatypeConverter.parseBase64Binary(SECRET_KEY);
			Key signingKey = new SecretKeySpec(apiKeySecretBytes, signatureAlgorithm.getJcaName());

			// Let's set the JWT Claims
			JwtBuilder builder = Jwts.builder().setId(otp.toString())
					.setIssuedAt(now).signWith(signatureAlgorithm,
					signingKey);

	        //if it has been specified, let's add the expiration
	        if (expiryMilliseconds >= 0) {
	            long expMillis = nowMillis + expiryMilliseconds;
	            Date exp = new Date(expMillis);
	            builder.setExpiration(exp);
	        }

			// Builds the JWT and serializes it to a compact, URL-safe string
			return builder.compact();
		}


	public static Claims decodeJWT(String jwt) {

		try {
			Claims claims = Jwts.parser().setSigningKey(DatatypeConverter.parseBase64Binary(SECRET_KEY))
					.parseClaimsJws(jwt).getBody();
			return claims;
		} catch (Exception e) {
			return null;
		}

	}

	/**
	 * this method is used to validate the user details
	 * 
	 * @param userId
	 * @param token
	 * @return
	 */

	public static boolean isValidUser(String token,int userId) {
		boolean result = false;
		Claims claims = decodeJWT(token);
		String tempId = "" + userId;
		if (claims != null) {
			String  tokenUserId = claims.getId();
			
			if (tokenUserId.equals(tempId)) {
				result = true;
			}
		}
		return result;
	}
	
	
	public static boolean isValidUser(String token) {
		boolean result = false;
		Claims claims = decodeJWT(token);
		
		if (claims != null) {
			String  tokenUserId = claims.getId();
			
			if (tokenUserId != null) {
				result = true;
			}
		}
		return result;
	}
	

}
