package com.way2walkins.dao;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import com.way2walkins.entities.User;

/**
 * This Interface is for EmployeeDao
 * 
 * @author IMVIZAG
 *
 */
public interface EmployeeDao extends CrudRepository<User, Integer> {

	List<User>findByRole(int value);

}
