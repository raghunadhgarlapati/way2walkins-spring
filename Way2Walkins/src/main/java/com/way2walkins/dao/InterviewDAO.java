package com.way2walkins.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import com.way2walkins.entities.Drive;
import com.way2walkins.entities.Interview;
import com.way2walkins.entities.User;


/**
 * 
 * this interface used to declare the methods to perform database operations on Interview table
 *
 */
public interface InterviewDAO  extends CrudRepository<Interview, Integer>{


	public List<Interview> findByInterviewer(User interviewer);
	
	public List<Interview> findByDrive(Drive drive);
	
	
	@Query(value = "Select i from Interview i where i.drive.driveId = :driveId and i.candidate.userId = :userId")
	public Interview findByDriveIdandUserId(int driveId,int userId);

}
