package com.way2walkins.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import com.way2walkins.entities.Drive;

/**
 * this interface used to declare the methods to retrive data from drives table
 * @author IMVIZAG
 *
 */
public interface DriveDao extends CrudRepository<Drive, Integer> {
	
	@Query(value = "SELECT d FROM Drive d WHERE d.tags like %:tag% ")
	public List<Drive> findByTag(String tag);
	
	@Query(value = "SELECT d FROM Drive d WHERE d.by.userId =:recruterid and  d.tags like %:tag% ")
	public List<Drive> searchByRecrId(String tag,int recruterid);
}
