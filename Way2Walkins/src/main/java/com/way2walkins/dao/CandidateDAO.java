package com.way2walkins.dao;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import com.way2walkins.entities.User;

/**
 * This Interface is for CandidateDAO
 * 
 * @author IMVIZAG
 *
 */

public interface CandidateDAO extends CrudRepository<User, Integer> {

	public User findByEmail(String email);

	public List<User> findByRole(int role);

}
