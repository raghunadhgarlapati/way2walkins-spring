package com.way2walkins.dao;

import org.springframework.data.repository.CrudRepository;

import com.way2walkins.entities.Registration;

/**
 *  this interface used to declare the methods to perform database operations on Registration table
 * @author IMVIZAG
 *
 */
public interface RegistrationDAO extends CrudRepository<Registration, Integer>{

}
