package com.way2walkins.dao;

import org.springframework.data.repository.CrudRepository;

import com.way2walkins.entities.FeedBack;

/**
 *   this interface used to declare the methods to retrive data from feedback table
 * @author IMVIZAG
 *
 */
public interface FeedBackDAO extends CrudRepository<FeedBack, Integer>{
		
}
