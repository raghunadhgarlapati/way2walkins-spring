package com.way2walkins.dao;

import org.springframework.data.repository.CrudRepository;

import com.way2walkins.entities.FeedBackDetails;


/**
 *  this interface used to declare the methods to perform database operations on FeedBackDetails table
 * @author IMVIZAG
 *
 */
public interface FeedBackDetailsDAO extends CrudRepository<FeedBackDetails, Integer>{

}
