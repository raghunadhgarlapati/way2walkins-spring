package com.way2walkins.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;
import com.way2walkins.entities.FeedBack;
import com.way2walkins.entities.Interview;
import com.way2walkins.services.InterviewerService;
import com.way2walkins.util.JwtImpl;
import com.way2walkins.util.ResponseType;
import com.way2walkins.util.StatusCodes;
import com.way2walkins.util.UnAuthenticationResponse;
import com.you_aspire.beens.InterviewerScheduledData;

/**
 * This class provides implementation for InterviewerController
 * 
 * @author Yo_Aspire
 *
 */

@RestController
@CrossOrigin

public class InterviewerControllerImpl {

	@Autowired
	InterviewerService service;

	@Autowired
	private ResponseType responseType;

	/**
	 * This method returns the all drives of the interviewers.
	 * 
	 * @param interviewer_id
	 * @return List<>
	 */
	@GetMapping("api/drives/interviewer/{interviewerId}")

	public ResponseEntity<ResponseType> getAllDrives(@RequestHeader("Authorization") String token,
			@PathVariable("interviewerId") int interviewer_id) {

		if (!JwtImpl.isValidUser(token, interviewer_id)) {
			return UnAuthenticationResponse.getResponse(token);
		}

		InterviewerScheduledData interviewScheduledData = null;
		try {
			interviewScheduledData = service.getAllInterviews(interviewer_id);
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (interviewScheduledData != null) {
			responseType.setResponse(interviewScheduledData);
			responseType.setStatusCode(StatusCodes.OK.getValue());
			responseType.setToken(token);
			return new ResponseEntity<ResponseType>(responseType, HttpStatus.OK);
		} else {
			responseType.setResponse("some thing went worng");
			responseType.setStatusCode(3);
			return new ResponseEntity<ResponseType>(responseType, HttpStatus.OK);
		}

	}

	/**
	 * used to give the feedback of the particular interview
	 * 
	 * @param driveId
	 * @param interviewerId
	 * @param userId
	 * @param feedback
	 * @return
	 */

	@PostMapping("api/drives/{driveId}/interviewer/{interviewerId}/giveFeedBack/{userId}")

	public ResponseEntity<ResponseType> giveFeedBack(@RequestHeader("Authorization") String token,
			@PathVariable("driveId") int driveId, @PathVariable("interviewerId") int interviewerId,
			@PathVariable("userId") int userId, @RequestBody FeedBack feedback) {

		if (!JwtImpl.isValidUser(token, interviewerId)) {
			return UnAuthenticationResponse.getResponse(token);
		}

		boolean status = false;
		try {
			status = service.giveFeedBack(driveId, userId, interviewerId, feedback);
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (status) {
			responseType.setResponse(StatusCodes.OK);
			responseType.setStatusCode(StatusCodes.OK.getValue());
			responseType.setToken(token);
			return new ResponseEntity<ResponseType>(responseType, HttpStatus.OK);
		} else {
			responseType.setResponse(StatusCodes.ERROR);
			responseType.setStatusCode(StatusCodes.ERROR.getValue());
			return new ResponseEntity<ResponseType>(responseType, HttpStatus.OK);
		}
	}

	/**
	 * this method is used to get the interview details
	 * 
	 * @param token
	 * @param interviewId
	 * @return
	 */
	@GetMapping("api/GetInterviewDetails/{interviewId}")

	public ResponseEntity<ResponseType> getInterviewDetails(@RequestHeader("Authorization") String token,
			@PathVariable("interviewId") int interviewId) {
		Interview interview = null;

		try {
			interview = service.getInterviewDetails(interviewId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (interview != null) {
			responseType.setResponse(interview);
			responseType.setStatusCode(StatusCodes.OK.getValue());
			responseType.setToken(token);
			return new ResponseEntity<ResponseType>(responseType, HttpStatus.OK);
		} else {
			responseType.setResponse(interview);
			responseType.setStatusCode(3);
			return new ResponseEntity<ResponseType>(responseType, HttpStatus.OK);
		}

	}

	/**
	 * used to
	 * 
	 * @param token
	 * @param interviewerId
	 * @param searchTag
	 * @return
	 */
	@GetMapping("api/searchByTag/{searchTag}/interviewer/{interviewerId}")

	public ResponseEntity<ResponseType> interviewerSearchByTag(@RequestHeader("Authorization") String token,
			@PathVariable("interviewerId") int interviewerId, @PathVariable("searchTag") String searchTag) {
		InterviewerScheduledData interviewerScheduledData = null;
		if (!JwtImpl.isValidUser(token, interviewerId)) {
			return UnAuthenticationResponse.getResponse(token);
		}
		try {
			interviewerScheduledData = service.searchByTag(searchTag, interviewerId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (interviewerScheduledData != null) {
			responseType.setResponse(interviewerScheduledData);
			responseType.setStatusCode(StatusCodes.OK.getValue());
			responseType.setToken(token);
			return new ResponseEntity<ResponseType>(responseType, HttpStatus.OK);

		} else {
			responseType.setResponse(StatusCodes.NOT_FOUND);
			responseType.setStatusCode(StatusCodes.NOT_FOUND.getValue());
			return new ResponseEntity<ResponseType>(responseType, HttpStatus.OK);
		}

	}
}
