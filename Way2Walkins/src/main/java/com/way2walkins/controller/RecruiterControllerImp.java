package com.way2walkins.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;
import com.way2walkins.entities.Drive;
import com.way2walkins.entities.FeedBackDetails;
import com.way2walkins.entities.Interview;
import com.way2walkins.services.RecruiterServices;
import com.way2walkins.util.JwtImpl;
import com.way2walkins.util.ResponseType;
import com.way2walkins.util.StatusCodes;
import com.way2walkins.util.UnAuthenticationResponse;
import com.you_aspire.beens.DriveBasicDto;
import com.you_aspire.beens.UserBasicDto;

@RestController
@CrossOrigin

/**
 * this class is providing the all the api for recruiter operations
 * 
 * @author IMVIZAG
 *
 */
public class RecruiterControllerImp {

	@Autowired
	private RecruiterServices recrutorService;

	@Autowired
	private ResponseType responseType;

	/**
	 * this method gives the drives list for the recruiter
	 * 
	 * @param recruterId
	 * @return
	 */
	@GetMapping("/api/recruiter/drives/{recruiterId}")
	public ResponseEntity<ResponseType> getRecruterDrivesList(@RequestHeader("Authorization") String token,
			@PathVariable("recruiterId") int recruterId) {

		if (!JwtImpl.isValidUser(token, recruterId)) {
			return UnAuthenticationResponse.getResponse(token);
		}

		List<DriveBasicDto> drivesList = null;
		try {
			drivesList = recrutorService.getRecruterDrivesList(recruterId);
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (drivesList != null) {
			responseType.setResponse(drivesList);
			responseType.setStatusCode(StatusCodes.OK.getValue());
			responseType.setToken(token);
			return new ResponseEntity<ResponseType>(responseType, HttpStatus.OK);
		} else {
			responseType.setResponse(StatusCodes.ERROR);
			responseType.setStatusCode(StatusCodes.ERROR.getValue());
			return new ResponseEntity<ResponseType>(responseType, HttpStatus.OK);

		}

	}

	/**
	 * this method creates the new Drive by this recruiter
	 * 
	 * @param userId
	 * @param drive
	 * @return
	 */
	@PostMapping("/api/create/drives/{userId}")
	public ResponseEntity<ResponseType> createDrive(@RequestHeader("Authorization") String token,
			@PathVariable("userId") int userId, @RequestBody Drive drive) {

		if (!JwtImpl.isValidUser(token, userId)) {
			return UnAuthenticationResponse.getResponse(token);
		}

		boolean status = false;
		// List<Drive> driveList = null;
		try {
			status = recrutorService.createDrive(userId, drive);
//			driveList = recrutorService.getRecruterDrivesList(userId);
		} catch (Exception e) {
			e.printStackTrace();
			status = false;
		}

		if (status) {
			responseType.setResponse("Drive created Successfully");
			responseType.setStatusCode(StatusCodes.OK.getValue());
			responseType.setToken(token);
			return new ResponseEntity<ResponseType>(responseType, HttpStatus.OK);
		} else {
			responseType.setResponse("Some thing wrong");
			responseType.setStatusCode(StatusCodes.ERROR.getValue());
			return new ResponseEntity<ResponseType>(responseType, HttpStatus.OK);
		}

	}

	/**
	 * this method is used to modify the data of the particular drive data by the
	 * recruiter
	 * 
	 * @param userId
	 * @param drive
	 * @return
	 */
	@PutMapping("/api/modifyDrive")

	public ResponseEntity<ResponseType> modifyDrive(@RequestHeader("Authorization") String token,
			@RequestBody Drive drive) {

		if (!JwtImpl.isValidUser(token, drive.getBy().getUserId())) {
			return UnAuthenticationResponse.getResponse(token);
		}
		boolean status = false;

		try {
			status = recrutorService.modifyDrive(drive);
		} catch (Exception e) {
			e.printStackTrace();
			status = false;
		}

		if (status) {
			responseType.setResponse("modified successfully");
			responseType.setStatusCode(StatusCodes.OK.getValue());
			responseType.setToken(token);
			return new ResponseEntity<ResponseType>(responseType, HttpStatus.OK);
		} else {
			responseType.setResponse("modify failed,some thing went wrong");
			responseType.setStatusCode(StatusCodes.ERROR.getValue());
			return new ResponseEntity<ResponseType>(responseType, HttpStatus.OK);
		}

	}

	/**
	 * this method is for the scheduling the particular Drive
	 * 
	 * @param driveId
	 * @param candidateId
	 * @param recruiterId
	 * @param interview
	 * @return
	 */
	@PostMapping("/api/schedule/drive/{driveId}/candidate/{candidateId}/recruiter/{recruiterId}")

	private ResponseEntity<ResponseType> scheduleDrive(@RequestHeader("Authorization") String token,
			@PathVariable("driveId") int driveId, @PathVariable("candidateId") int candidateId,
			@PathVariable("recruiterId") int recruiterId, @RequestBody Interview interview) {
		if (!JwtImpl.isValidUser(token, recruiterId)) {
			return UnAuthenticationResponse.getResponse(token);
		}
		boolean status = false;
		try {
			status = recrutorService.scheduleDrive(driveId, candidateId, interview, recruiterId);
		} catch (Exception e) {
			e.printStackTrace();
			status = false;
		}
		if (status) {
			responseType.setResponse("Scheduled successfully");
			responseType.setStatusCode(StatusCodes.OK.getValue());
			return new ResponseEntity<ResponseType>(responseType, HttpStatus.OK);
		}

		else {
			responseType.setResponse("something wrong");
			responseType.setStatusCode(StatusCodes.ERROR.getValue());
			responseType.setToken(token);
			return new ResponseEntity<ResponseType>(responseType, HttpStatus.OK);
		}
	}

	/**
	 * this method reschedule the user Reschedule information which is already
	 * scheduled
	 * 
	 * @param driveId
	 * @param candidateId
	 * @param recruiterId
	 * @param interview
	 * @return
	 */
	@PutMapping("/api/reschedule")
	public ResponseEntity<ResponseType> reScheduleDrive(@RequestHeader("Authorization") String token,
			@RequestBody Interview interview) {

		if (!JwtImpl.isValidUser(token, interview.getRecruiter().getUserId())) {
			return UnAuthenticationResponse.getResponse(token);
		}
		boolean status = false;
		try {
			status = recrutorService.reScheduleDrive(interview);
		} catch (Exception e) {
			status = false;
			e.printStackTrace();
		}

		if (status) {
			responseType.setResponse("rescheduled successfully");
			responseType.setStatusCode(StatusCodes.OK.getValue());
			responseType.setToken(token);
			return new ResponseEntity<ResponseType>(responseType, HttpStatus.OK);
		}

		else {
			responseType.setResponse("some thing went wrong");
			responseType.setStatusCode(StatusCodes.ERROR.getValue());
			return new ResponseEntity<ResponseType>(responseType, HttpStatus.OK);
		}
	}

	/**
	 * this method is used to get the schedule details for the particular recruiter
	 * 
	 * @param token
	 * @param driveId
	 * @param candidateId
	 * @param recrid
	 * @return
	 */
	@GetMapping("/api/getSchedule/drive/{driveId}/candidate/{candidateId}/recruiter/{recrid}")
	public ResponseEntity<ResponseType> getScheduleObject(@RequestHeader("Authorization") String token,
			@PathVariable("driveId") int driveId, @PathVariable("candidateId") int candidateId,
			@PathVariable("recrid") int recrid) {

		if (!JwtImpl.isValidUser(token, recrid)) {
			return UnAuthenticationResponse.getResponse(token);
		}
		Interview interview = null;

		try {
			interview = recrutorService.getScheduleObject(driveId, candidateId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (interview != null) {
			responseType.setResponse(interview);
			responseType.setStatusCode(StatusCodes.OK.getValue());
			responseType.setToken(token);
			return new ResponseEntity<ResponseType>(responseType, HttpStatus.OK);
		}

		else {
			responseType.setResponse(StatusCodes.ERROR);
			responseType.setStatusCode(StatusCodes.ERROR.getValue());
			return new ResponseEntity<ResponseType>(responseType, HttpStatus.OK);
		}
	}

	/**
	 * this method is used to set the feedback details of a particular drive by a
	 * recruiter
	 * 
	 * @param driveId
	 * @param         List<feedBackDetails>
	 */

	@CrossOrigin
	@PostMapping("/api/drives/{driveId}/setFeedBackDetails/recruiter/{recruiterId}")
	public ResponseEntity<ResponseType> setFeedBackDetails(@RequestHeader("Authorization") String token,
			@PathVariable("driveId") int driveId, @PathVariable("recruiterId") int recruiterId,
			@RequestBody List<FeedBackDetails> feedBackDetails) {

		if (!JwtImpl.isValidUser(token, recruiterId)) {
			return UnAuthenticationResponse.getResponse(token);
		}

		boolean status = false;
		try {
			status = recrutorService.setFeedBackDetails(driveId, feedBackDetails);
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (status) {
			responseType.setResponse(status);
			responseType.setStatusCode(StatusCodes.OK.getValue());
			responseType.setToken(token);
			return new ResponseEntity<ResponseType>(responseType, HttpStatus.OK);
		} else {
			responseType.setResponse(status);
			responseType.setStatusCode(StatusCodes.ERROR.getValue());
			return new ResponseEntity<ResponseType>(responseType, HttpStatus.OK);
		}

	}

	/**
	 * this method returns the total interviewers list
	 */
	@GetMapping("/api/getInterviewers")

	public ResponseEntity<ResponseType> getAllInterviewers(@RequestHeader("Authentication") String token) {

		List<UserBasicDto> interviewers = null;

		try {
			interviewers = recrutorService.getAllInterviewers();
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (interviewers != null) {
			responseType.setResponse(interviewers);
			responseType.setStatusCode(StatusCodes.OK.getValue());

			return new ResponseEntity<ResponseType>(responseType, HttpStatus.OK);
		} else {
			responseType.setResponse(interviewers);
			responseType.setStatusCode(StatusCodes.NOT_FOUND.getValue());
			return new ResponseEntity<ResponseType>(responseType, HttpStatus.NOT_FOUND);
		}
	}

	/**
	 * this method is used to make completion of drive status
	 * 
	 * @param token
	 * @param driveId
	 * @return
	 */

	@PutMapping("/api/makeDriveComplete/{driveId}/recruiter/{recruiterId}")
	@CrossOrigin

	public ResponseEntity<ResponseType> makeDriveComplete(@RequestHeader("Authorization") String token,
			@PathVariable("driveId") int driveId, @PathVariable("recruiterId") int recruiterId) {
		if (!JwtImpl.isValidUser(token, recruiterId)) {
			return UnAuthenticationResponse.getResponse(token);
		}
		boolean status = recrutorService.makeDriveComplete(driveId);
		if (status) {
			responseType.setResponse("drive completed sucessfully");
			responseType.setStatusCode(StatusCodes.OK.getValue());
			responseType.setToken(token);
			return new ResponseEntity<ResponseType>(responseType, HttpStatus.OK);
		} else {
			responseType.setResponse("something went wrong");
			responseType.setStatusCode(StatusCodes.ERROR.getValue());
			return new ResponseEntity<ResponseType>(responseType, HttpStatus.OK);
		}

	}

	 /**
	  * this method is used to get drives with search tag
	  * @param token
	  * @param searchTag
	  * @param recruiterId
	  * @return
	  */
	@GetMapping("api/search/{searchTag}/recruiter/{recruiterId}")
	public ResponseEntity<ResponseType> searchByTag(@RequestHeader("Authorization") String token,
			@PathVariable("searchTag") String searchTag, @PathVariable("recruiterId") int recruiterId) {
		if (!JwtImpl.isValidUser(token, recruiterId)) {
			return UnAuthenticationResponse.getResponse(token);
		}

		List<DriveBasicDto> drivesList = null;
		try {
			drivesList = recrutorService.searchByTag(searchTag, recruiterId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (drivesList != null) {
			responseType.setResponse(drivesList);
			responseType.setStatusCode(StatusCodes.OK.getValue());
			responseType.setToken(token);
			return new ResponseEntity<ResponseType>(responseType, HttpStatus.OK);

		} else {
			responseType.setResponse(StatusCodes.NOT_FOUND);
			responseType.setStatusCode(StatusCodes.NOT_FOUND.getValue());
			return new ResponseEntity<ResponseType>(responseType, HttpStatus.OK);
		}
	}

	 /**
	  * this method is used to get the list of  recruiters
	  * @return
	  */
	@GetMapping("/api/getAllRecruiters")

	public ResponseEntity<ResponseType> getRecruiters() {
		List<UserBasicDto> recruiters = null;

		try {
			recruiters = recrutorService.getAllRecruiters();
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (recruiters != null) {
			responseType.setResponse(recruiters);
			responseType.setStatusCode(StatusCodes.OK.getValue());
			return new ResponseEntity<ResponseType>(responseType, HttpStatus.OK);
		} else {
			responseType.setResponse(recruiters);
			responseType.setStatusCode(StatusCodes.NOT_FOUND.getValue());
			return new ResponseEntity<ResponseType>(responseType, HttpStatus.NOT_FOUND);
		}
	}

}
