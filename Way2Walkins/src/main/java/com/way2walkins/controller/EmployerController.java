package com.way2walkins.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;
import com.way2walkins.services.EmployerService;
import com.way2walkins.util.JwtImpl;
import com.way2walkins.util.ResponseType;
import com.way2walkins.util.StatusCodes;
import com.way2walkins.util.UnAuthenticationResponse;
import com.you_aspire.beens.DriveBasicDto;
import com.you_aspire.beens.DriveInterviews;
import com.you_aspire.beens.UserBasicDto;

@RestController
@CrossOrigin
/**
 * this class handles all the api of the employee operations
 * 
 * @author IMVIZAG
 *
 */
public class EmployerController {

	@Autowired
	private EmployerService employerService;
	@Autowired
	ResponseType responseType;

	/**
	 * this method returns all the drives created by the employer
	 * 
	 * @PathVariable employerId.
	 */
	@CrossOrigin
	@GetMapping("api/drives/employer/{employerId}")

	public ResponseEntity<ResponseType> getEmployerDrives(@RequestHeader("Authorization") String token,
			@PathVariable("employerId") int employerId) {

		if (!JwtImpl.isValidUser(token, employerId)) {
			return UnAuthenticationResponse.getResponse(token);
		}

		List<DriveBasicDto> drives = null;
		try {
			drives = employerService.getDrives(employerId);
		} catch (Exception e) {

			e.printStackTrace();
		}
		if (drives != null) {
			responseType.setResponse(drives);
			responseType.setStatusCode(StatusCodes.OK.getValue());
			responseType.setToken(token);
			return new ResponseEntity<ResponseType>(responseType, HttpStatus.OK);

		} else {
			responseType.setResponse(StatusCodes.ERROR);
			responseType.setStatusCode(StatusCodes.ERROR.getValue());
			return new ResponseEntity<ResponseType>(responseType, HttpStatus.OK);
		}
	}

	/**
	 * this method is used to get the interviews with particular driveId
	 * 
	 * @param driveId
	 * @return
	 */

	@GetMapping("api/employer/getInterviews/{driveId}/{employerId}")
	@CrossOrigin

	public ResponseEntity<ResponseType> getInterviewsList(@RequestHeader("Authorization") String token,
			@PathVariable("driveId") int driveId, @PathVariable("employerId") int employerId) {

		if (!JwtImpl.isValidUser(token, employerId)) {
			return UnAuthenticationResponse.getResponse(token);
		}
		DriveInterviews driveInterviews = null;
		try {
			driveInterviews = employerService.getInterviewsList(driveId);
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (driveInterviews != null) {
			responseType.setResponse(driveInterviews);
			responseType.setStatusCode(StatusCodes.OK.getValue());
			responseType.setToken(token);
			return new ResponseEntity<ResponseType>(responseType, HttpStatus.OK);
		} else {
			responseType.setResponse(StatusCodes.ERROR);
			responseType.setStatusCode(StatusCodes.ERROR.getValue());
			return new ResponseEntity<ResponseType>(responseType, HttpStatus.OK);
		}
	}

	/**
	 * this method is used to give the list of employee's
	 * 
	 * @param token
	 * @param driveId
	 * @return
	 */
	@GetMapping("api/employer/getAllEmployees")
	@CrossOrigin

	public ResponseEntity<ResponseType> getAllEmployee() {
		List<UserBasicDto> employers = null;
		try {
			employers = employerService.getAllEmployers();
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (employers != null) {
			responseType.setResponse(employers);
			responseType.setStatusCode(StatusCodes.OK.getValue());

			return new ResponseEntity<ResponseType>(responseType, HttpStatus.OK);
		} else {
			responseType.setResponse(employers);
			responseType.setStatusCode(StatusCodes.NOT_FOUND.getValue());
			return new ResponseEntity<ResponseType>(responseType, HttpStatus.NOT_FOUND);
		}

	}

	/***
	 * this method is used to get the list of drives of the of the employer with
	 * particular tag search
	 * 
	 * @param token
	 * @param searchTag
	 * @param employerId
	 * @return
	 */
	@GetMapping("api/search/{searchTag}/employer/{employerId}")

	public ResponseEntity<ResponseType> searchByTag(@RequestHeader("Authorization") String token,
			@PathVariable("searchTag") String searchTag, @PathVariable("employerId") int employerId) {
		if (!JwtImpl.isValidUser(token, employerId)) {
			return UnAuthenticationResponse.getResponse(token);
		}

		List<DriveBasicDto> drivesList = null;
		try {
			drivesList = employerService.searchByTag(searchTag, employerId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (drivesList != null) {
			responseType.setResponse(drivesList);
			responseType.setStatusCode(StatusCodes.OK.getValue());
			responseType.setToken(token);
			return new ResponseEntity<ResponseType>(responseType, HttpStatus.OK);

		} else {
			responseType.setResponse(StatusCodes.NOT_FOUND);
			responseType.setStatusCode(StatusCodes.NOT_FOUND.getValue());
			return new ResponseEntity<ResponseType>(responseType, HttpStatus.OK);
		}
	}
}
