package com.way2walkins.controller;

import javax.servlet.ServletContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;
import com.way2walkins.configurations.UserPasswordEncription;
import com.way2walkins.entities.Drive;
import com.way2walkins.entities.LoginModel;
import com.way2walkins.entities.User;
import com.way2walkins.services.CandidateServices;
import com.way2walkins.util.EmailVerfication;
import com.way2walkins.util.JwtImpl;
import com.way2walkins.util.ResponseType;
import com.way2walkins.util.StatusCodes;
import com.way2walkins.util.UnAuthenticationResponse;
import com.you_aspire.beens.CandidateTotalDrives;
import com.you_aspire.beens.UserBasicDto;

/**
 * This class implements the candidate registration details.
 * 
 * @author yo_Aspire
 *
 */
@RestController
@CrossOrigin
public class CandidateControllerImpl {

	@Autowired
	CandidateServices service;

	@Autowired
	ServletContext servletContext;

	@Autowired
	private ResponseType responseType;

	@Autowired
	private EmailVerfication emailVerfication;

	/**
	 * This method is used to register the candidate with credentials like unique
	 * emailId.
	 * 
	 * @return
	 */

	@PostMapping("/api/register")

	public ResponseEntity<ResponseType> registerCandidate(@RequestBody User user) {
		ResponseEntity<ResponseType> responseEntity = null;
		boolean isexisted = service.isExisted(user.getEmail());
		if (isexisted) {
			responseType.setResponse("User already exists with this email ");
			responseType.setStatusCode(StatusCodes.ALREADY_EXIST.getValue());
			responseEntity = new ResponseEntity<ResponseType>(responseType, HttpStatus.OK);
			return responseEntity;
		}

		int otp = EmailVerfication.generateRandomNumber();
		System.out.println("otp" + otp);
		String token = JwtImpl.createJWT(otp);
		boolean sendEmail = emailVerfication.sendMail(user.getEmail(), otp);
		if (sendEmail) {
			responseType.setResponse(user);
			responseType.setStatusCode(1);
			responseType.setToken(token);
			responseEntity = new ResponseEntity<ResponseType>(responseType, HttpStatus.OK);
		} else {
			responseType.setResponse("email Send failed");
			responseType.setStatusCode(2);

			responseEntity = new ResponseEntity<ResponseType>(responseType, HttpStatus.OK);
		}
		return responseEntity;
	}

	/**
	 * method is used to verify the otp that has been sent to the email
	 * 
	 * @param token
	 * @param otp
	 * @param user
	 * @return
	 */

	@PostMapping("/api/register/verifyEmail/{otp}")
	public ResponseEntity<ResponseType> registerCandidate(@RequestHeader("Authorization") String token,
			@PathVariable("otp") int otp, @RequestBody User user) {

		// verify token whether it is valid or not
		if (!JwtImpl.isValidUser(token, otp)) {
			return UnAuthenticationResponse.getResponse(token);
		}

		ResponseEntity<ResponseType> responseEntity = null;
		StatusCodes status = null;

		try {
			String encriptedPassword = UserPasswordEncription.getEncryptedPassword(user.getPassword());
			user.setPassword(encriptedPassword);
			System.out.println(user.getEmail());
			status = service.candidateRegistration(user);
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (status != null) {
			responseType.setResponse(status);
			responseType.setStatusCode(status.getValue());
			responseType.setToken(token);
			responseEntity = new ResponseEntity<ResponseType>(responseType, HttpStatus.OK);
		} else {
			responseType.setResponse("fail");
			responseType.setStatusCode(StatusCodes.ERROR.getValue());
			responseEntity = new ResponseEntity<ResponseType>(responseType, HttpStatus.OK);
		}

		return responseEntity;

	}

	/**
	 * this method checks the user authentication with the emailId and password
	 * 
	 * @param loginData
	 * @return
	 */

	@PostMapping("/api/login")
	public ResponseEntity<ResponseType> LoginCandidate(@RequestBody LoginModel loginData) {
		try {
			UserBasicDto userBasic = service.candidateLogin(loginData);

			if (userBasic != null) {
				@SuppressWarnings("static-access")

				String token = new JwtImpl().createJWT(userBasic.getUserId());
				responseType.setResponse(userBasic);
				responseType.setStatusCode(StatusCodes.OK.getValue());
				responseType.setToken(token);
				return new ResponseEntity<ResponseType>(responseType, HttpStatus.OK);

			} else {
				responseType.setResponse(StatusCodes.NOT_FOUND);
				responseType.setStatusCode(StatusCodes.NOT_FOUND.getValue());
				return new ResponseEntity<ResponseType>(responseType, HttpStatus.OK);
			}

		} catch (Exception e) {
			e.printStackTrace();
			responseType.setResponse(e.getMessage());
			responseType.setStatusCode(StatusCodes.NOT_REGISTER.getValue());
			return new ResponseEntity<ResponseType>(responseType, HttpStatus.OK);
		}

	}

	/**
	 * this method returns the suggested and registered drives
	 * 
	 * @param token
	 * @param candidateId
	 * @return
	 */

	@GetMapping("/api/drives/candidate/{userId}")
	public ResponseEntity<ResponseType> getCandidateDrives(@RequestHeader("Authorization") String token,
			@PathVariable("userId") int candidateId) {

		// token validation
		if (!JwtImpl.isValidUser(token, candidateId)) {
			return UnAuthenticationResponse.getResponse(token);
		}

		try {
			CandidateTotalDrives candidateTotalDrives = service.getCandidateDrives(candidateId);
			responseType.setResponse(candidateTotalDrives);
			responseType.setStatusCode(StatusCodes.OK.getValue());
			responseType.setToken(token);
			return new ResponseEntity<ResponseType>(responseType, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			responseType.setResponse(StatusCodes.ERROR);
			responseType.setStatusCode(StatusCodes.ERROR.getValue());
			return new ResponseEntity<ResponseType>(responseType, HttpStatus.OK);
		}

	}

	/**
	 * 
	 * this method register the user to the particular drive
	 * 
	 * @param userId
	 * @param driveId
	 * @return
	 */
	@CrossOrigin
	@GetMapping("api/drives/{driveId}/register/{userId}")

	public ResponseEntity<ResponseType> userRegisterToDrive(@RequestHeader("Authorization") String token,
			@PathVariable("userId") int userId, @PathVariable("driveId") int driveId) {

		if (!JwtImpl.isValidUser(token, userId)) {
			return UnAuthenticationResponse.getResponse(token);
		}

		boolean status = false;
		try {
			status = service.userRegisterToDrive(userId, driveId);
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (status) {
			responseType.setResponse(status);
			responseType.setStatusCode(StatusCodes.OK.getValue());
			responseType.setToken(token);
			return new ResponseEntity<ResponseType>(responseType, HttpStatus.OK);

		} else {
			responseType.setResponse(StatusCodes.ERROR);
			responseType.setStatusCode(StatusCodes.ERROR.getValue());
			return new ResponseEntity<ResponseType>(responseType, HttpStatus.OK);
		}

	}

	/**
	 * this method returns the single Drive of the given drive Id
	 * 
	 * @param driveId
	 * 
	 * @return
	 */

	@GetMapping("api/drives/{driveId}/{userId}")

	public ResponseEntity<ResponseType> getDrive(@RequestHeader("Authorization") String token,
			@PathVariable("driveId") int driveId, @PathVariable("userId") int userId) {

		if (!JwtImpl.isValidUser(token, userId)) {
			return UnAuthenticationResponse.getResponse(token);
		}
		Drive drive = null;
		try {
			drive = service.getDrive(driveId, userId);
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (drive != null) {
			responseType.setResponse(drive);
			responseType.setStatusCode(StatusCodes.OK.getValue());
			responseType.setToken(token);
			return new ResponseEntity<ResponseType>(responseType, HttpStatus.OK);

		} else {
			responseType.setResponse(StatusCodes.NOT_FOUND);
			responseType.setStatusCode(StatusCodes.NOT_FOUND.getValue());
			return new ResponseEntity<ResponseType>(responseType, HttpStatus.OK);
		}

	}

	/**
	 * this method is used to display the list of drives with particular searching
	 * string which are related with that string
	 * 
	 * @param searchTag
	 * @return
	 */
	@GetMapping("api/search/{searchTag}/candidate/{candidateId}")

	public ResponseEntity<ResponseType> searchByTag(@RequestHeader("Authorization") String token,
			@PathVariable("searchTag") String searchTag, @PathVariable("candidateId") int candidateId) {
		if (!JwtImpl.isValidUser(token, candidateId)) {
			return UnAuthenticationResponse.getResponse(token);
		}

		CandidateTotalDrives drivesList = null;
		try {
			drivesList = service.searchByTag(searchTag, candidateId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (drivesList != null) {
			responseType.setResponse(drivesList);
			responseType.setStatusCode(StatusCodes.OK.getValue());
			responseType.setToken(token);
			return new ResponseEntity<ResponseType>(responseType, HttpStatus.OK);

		} else {
			responseType.setResponse(StatusCodes.NOT_FOUND);
			responseType.setStatusCode(StatusCodes.NOT_FOUND.getValue());
			return new ResponseEntity<ResponseType>(responseType, HttpStatus.OK);
		}
	}

	/**
	 * this method is used to get the user object with the given userId
	 * 
	 * @param userId
	 */
	@GetMapping("api/GetUserInfo/{userId}")

	public ResponseEntity<ResponseType> GetUserInfo(@RequestHeader("Authorization") String token,
			@PathVariable("userId") int userId) {

		if (!JwtImpl.isValidUser(token)) {
			return UnAuthenticationResponse.getResponse(token);
		}

		User user = null;
		try {
			// find by id with userId
			user = service.GetUserInfo(userId);
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (user != null) {
			responseType.setResponse(user);
			responseType.setStatusCode(StatusCodes.OK.getValue());
			return new ResponseEntity<ResponseType>(responseType, HttpStatus.OK);
		} else {
			responseType.setResponse(StatusCodes.NOT_FOUND);
			responseType.setStatusCode(StatusCodes.NOT_FOUND.getValue());
			return new ResponseEntity<ResponseType>(responseType, HttpStatus.OK);
		}

	}

	@GetMapping("encript/{input}")

	public String encript(@PathVariable("input") String input) {
		return UserPasswordEncription.getEncryptedPassword(input);

	}

	@PutMapping("api/editUserInfo")

	public ResponseEntity<ResponseType> editUserInfo(@RequestHeader("Authorization") String token,
			@RequestBody User user) {
		if (!JwtImpl.isValidUser(token, user.getUserId())) {
			return UnAuthenticationResponse.getResponse(token);
		}

		User userStatus = null;
		try {

			userStatus = service.editUserInfo(user);
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (userStatus != null) {
			responseType.setResponse(user);
			responseType.setStatusCode(StatusCodes.OK.getValue());
			return new ResponseEntity<ResponseType>(responseType, HttpStatus.OK);
		} else {
			responseType.setResponse(StatusCodes.NOT_FOUND);
			responseType.setStatusCode(StatusCodes.NOT_FOUND.getValue());
			return new ResponseEntity<ResponseType>(responseType, HttpStatus.OK);
		}

	}

}
