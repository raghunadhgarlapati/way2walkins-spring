package com.you_aspire.beens;

import java.util.List;

/**
 * this class used set the candidate drives
 */


public class CandidateTotalDrives {

	List<DriveBasicDto> regiteredDrives;
	List<DriveBasicDto> suggestedDrives;

	public List<DriveBasicDto> getRegiteredDrives() {
		return regiteredDrives;
	}

	public void setRegiteredDrives(List<DriveBasicDto> regiteredDrives) {
		this.regiteredDrives = regiteredDrives;
	}

	public List<DriveBasicDto> getSuggestedDrives() {
		return suggestedDrives;
	}

	public void setSuggestedDrives(List<DriveBasicDto> suggestedDrives) {
		this.suggestedDrives = suggestedDrives;
	}

}
