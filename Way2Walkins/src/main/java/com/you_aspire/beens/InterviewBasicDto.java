package com.you_aspire.beens;

import java.sql.Date;

import com.way2walkins.entities.FeedBack;

public class InterviewBasicDto {
        
		private UserBasicDto candidate;
		private FeedBack feedBack;
		private Date when_interviewed;
		private String exactly;
		private boolean is_conducted;

		public UserBasicDto getCandidate() {
			return candidate;
		}

		public void setCandidate(UserBasicDto candidates) {
			this.candidate = candidates;
		}

		public FeedBack getFeedBack() {
			return feedBack;
		}

		public void setFeedBack(FeedBack feedBack) {
			this.feedBack = feedBack;
		}

		public Date getWhen_interviewed() {
			return when_interviewed;
		}

		public void setWhen_interviewed(Date when_interviewed) {
			this.when_interviewed = when_interviewed;
		}

		public String getExactly() {
			return exactly;
		}

		public void setExactly(String exactly) {
			this.exactly = exactly;
		}

		public boolean isIs_conducted() {
			return is_conducted;
		}

		public void setIs_conducted(boolean is_conducted) {
			this.is_conducted = is_conducted;
		}
	
	
	
}
